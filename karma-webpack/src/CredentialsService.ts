
export type CredentialsService = {};
export type CredentialsSubmitFn = (credentials: Credentials) => null | string;
export type Credentials = {
    email: string;
    password: string;
};
