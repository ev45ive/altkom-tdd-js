import $ from 'jquery';
import { CredentialsSubmitFn } from './CredentialsService';

// https://www.npmjs.com/package/jasmine-jquery-matchers
export class FormView {
    onSubmit: CredentialsSubmitFn = () => null

    constructor(private el: HTMLDivElement | JQuery<HTMLDivElement>) {
        $(this.el).on('submit', 'form', event => {
            event.preventDefault()

            const data = new FormData(event.target)

            const error = this.onSubmit({
                email: data.get('email')!.toString(),
                password: data.get('password')!.toString()
            })
            
            $(this.el).find('.message').prop('hidden', true)
            if (error) {
                $(this.el).find('.message').prop('hidden', false)
                $(this.el).find('.message').text(error)
            }
        })
    }
    render() {
        $(this.el).html(/* html */ `<form>
            <p class="message alert alert-danger" hidden></p>

            <div class="form-group mb-3">
                <input type="text" name="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group mb-3">
                <input type="text" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group mb-3">
                <button type="submit">Login</button>
            </div>

        </form>
        `);
    }
}
