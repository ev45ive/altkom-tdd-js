export type ValidatorFn = (value: string) => string | null

export class Validators {
    static combineValidator(validators: ValidatorFn[]): ValidatorFn {
        return (value: string) => validators.reduce((error, validatorFn) =>
            error ? error : validatorFn(value), null as ReturnType<ValidatorFn>)
    }

    static minlength = (minlength: number): ValidatorFn => {
        return (value) => {
            if (value.length === 0) return null
            if (value.length >= minlength) return null

            return `Minumum field length is ${minlength}`
        }
    }

    static email: ValidatorFn = (value) => {
        if (value.includes('@')) return null

        return 'Email is invalid'
    }

    static required: ValidatorFn = (value) => {
        if (value.trim() !== '') return null

        return 'Field is required';
    }
}