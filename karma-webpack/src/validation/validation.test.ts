import { ValidatorFn, Validators } from "./Validators";

describe('Form Validation', () => {

    let validators: typeof Validators

    beforeEach(() => {
        validators = Validators
    })

    it('required field shows "Field is required" error message', () => {
        expect(validators.required(' ')).toEqual('Field is required')
        expect(validators.required('I am not empty')).toBe(null)
        expect(validators.required('  ')).toBe('Field is required')
    })

    it('email field shows "Email is invalid" error message', () => {
        expect(validators.email('not an email')).toBe('Email is invalid')
        expect(validators.email('this.is@email.com')).toBe(null)
        expect(validators.email('an@email')).toBe(null)
    })

    it('shows number of required characters for minlength field', () => {
        expect(validators.minlength(3)('1')).toBe('Minumum field length is 3')
        expect(validators.minlength(5)('1')).toBe('Minumum field length is 5')
        expect(validators.minlength(3)('123')).toBe(null)
    })

    it('ignores minlength field for optional fields', () => {
        expect(validators.minlength(3)('')).toBe(null)
    })

    it('field shows only first of validation errors', () => {

        const NullValidator = jasmine.createSpy<ValidatorFn>('NullValidator')
            .and.returnValue(null)
        const ErrorValidator1 = jasmine.createSpy<ValidatorFn>('FakeErrorValidator')
            .and.returnValue('Fake Error 1')
        const ErrorValidator2 = jasmine.createSpy<ValidatorFn>('FakeErrorValidator')
            .and.returnValue('Fake Error 2')

        const multiValidator = Validators.combineValidator([
            NullValidator,
            ErrorValidator1,
            ErrorValidator2,
            // Validators.minlength(3)
            // MyValidators.pesel()
        ])
        expect(multiValidator('value breaks validator1')).toEqual('Fake Error 1')
        expect(NullValidator).toHaveBeenCalledWith('value breaks validator1')
        expect(ErrorValidator1).toHaveBeenCalledWith('value breaks validator1')
        // expect(ErrorValidator2).not.toHaveBeenCalledWith('value breaks validator1') // detail!

        ErrorValidator1.and.returnValue(null) // Validator1 is now OK (no error)
        expect(multiValidator('value breaks validator2')).toEqual('Fake Error 2')
        expect(ErrorValidator2).toHaveBeenCalledWith('value breaks validator2')

        ErrorValidator2.and.returnValue(null) // Validator2 is now OK (no error)
        expect(multiValidator('value breaks validator2')).toEqual(null)

    })

    it('sequence validators on all fields', () => { })

})

