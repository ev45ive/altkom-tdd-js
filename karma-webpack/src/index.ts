console.log("Hello World!");

import $ from 'jquery'
import { Credentials } from './CredentialsService';
import { FormView } from './FormView';

const form = new FormView($('#form'))
form.render()

form.onSubmit = (cred: Credentials) => {

    if(cred.email !== 'placki') return 'Bad username'
    if(cred.password !== 'placki') return 'Bad password'

    window.alert('Zalogowano!')

    return null
}