import $ from 'jquery';
import 'jasmine-jquery';
import { FormView } from './FormView';
import { Credentials, CredentialsSubmitFn } from "./CredentialsService";
describe('User Login Form', () => {
    let formContainerEl: JQuery<HTMLDivElement>
    let formView: FormView

    beforeEach(() => {
        formContainerEl = $(document.createElement('div'))
        formContainerEl.prop('id', 'test-container')
        document.body.append(formContainerEl[0])

        formView = new FormView(formContainerEl)
        formView.render()
    })
    afterEach(() => {
        $('#test-container').remove()
    })

    it('has empty fields for email and password', () => {
        expect(formContainerEl).toContainElement('input[name=email]')
        expect(formContainerEl).toContainElement('input[name=password]')
    });

    it('has "Login" button', () => {
        expect(formContainerEl).toContainElement('button[type=submit]')
        expect(formContainerEl.find('button[type=submit]')).toHaveText('Login')
    });

    it('when user clicks "Login" validate credentials and redirect', () => {
        const loginSpy = jasmine.createSpy<CredentialsSubmitFn>('FakeServiceMethod')
            .and.returnValue(null);
        formView.onSubmit = loginSpy

        formContainerEl.find('input[name=email]').val('placki@placki.com')
        formContainerEl.find('input[name=password]').val('tajneplacki')

        formContainerEl.find('button[type=submit]').trigger('click')

        expect(loginSpy).toHaveBeenCalledWith(jasmine.objectContaining({
            email: 'placki@placki.com',
            password: 'tajneplacki'
        }))
        // expect(loginSpy).toHaveBeenCalledWith(jasmine.objectContaining({
        //     email: jasmine.any(String),
        //     password: jasmine.anything()
        // }))
    });

    it('when user clicks "Login" validate credentials and show validation error', () => {
        const loginSpy = jasmine.createSpy<CredentialsSubmitFn>('FakeServiceMethod')
            .and.returnValue('User does not exist fake error');
        expect(formContainerEl.find('.message')).not.toBeVisible()
        formView.onSubmit = loginSpy

        formContainerEl.find('input[name=email]').val('placki@placki.com')
        formContainerEl.find('input[name=password]').val('tajneplacki')

        formContainerEl.find('button[type=submit]').trigger('click')

        expect(formContainerEl.find('.message')).toBeVisible()
        expect(formContainerEl.find('.message')).toContainText('User does not exist fake error')
    });

    it('shows username and password validation messages while typing', () => { })
})