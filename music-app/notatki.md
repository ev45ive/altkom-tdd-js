
## GIT 
cd ..
git clone https://bitbucket.org/ev45ive/altkom-tdd-js.git
cd altkom-tdd-js
cd karma-webpack
npm i 
npm test


# Instalacje
node -v 
v14.17.0

npm -v
6.14.6

code -v
1.62.1

git --version
git version 2.31.1.windows.1

chrome://version
Google Chrome	95.0.4638.69 

## Extensions
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

## Jasmine + Karma
<!-- Create dir -->
mkdir jasmine-karma 
cd jasmine-karma 
<!-- initialize package.json -->
npm init -y
<!-- install global karma Command Line Interface (remote control) -->
npm install -g karma-cli
<!-- => C:\Users\<username>\AppData\Roaming\npm\karma  -->
<!-- %PATH% == ...C:\Users\<username>\AppData\Roaming\npm\; ....  -->

karma --help
Cannot find local Karma!

npm install karma-jasmine karma-chrome-launcher jasmine-core karma --save-dev 
<!-- pagkage.json updated, node_modules updated -->

karma --help
Karma - Spectacular Test Runner for JavaScript.

Run --help with particular command to see its description and available options.

Usage:
  karma <command>

Polecenia:
  karma init [configFile]   Initialize a config file.
  karma start [configFile]  Start the server / do a single run.
  karma run [configFile]    Trigger a test run.
  karma stop [configFile]   Stop the server.
  karma completion          Shell completion for karma.

Opcje:
  --help     Print usage and options.                                  [boolean]
  --version  Print current version.                                    [boolean]

mkdir -p src/
touch src/index.js # modules
touch src/main.js # Entry point

mkdir -p test/
touch test/index.test.js # tests

karma init 

Which testing framework do you want to use ?
Press tab to list possible options. Enter to move to the next question.
> jasmine

Do you want to use Require.js ?
This will add Require.js plugin.
Press tab to list possible options. Enter to move to the next question.
> no

Do you want to capture any browsers automatically ?
Press tab to list possible options. Enter empty string to move to the next question.
> Chrome
> ChromeHeadless
>

What is the location of your source and test files ?
You can use glob patterns, eg. "js/*.js" or "test/**/*Spec.js".
Enter empty string to move to the next question.
> src/**/*.js
> test/**/*.test.js
>

Should any of the files included by the previous patterns be excluded ?
You can use glob patterns, eg. "**/*.swp".
Enter empty string to move to the next question.
> src/main.js
17 11 2021 11:29:16.110:WARN [init]: There is no file matching this pattern.

>

Do you want Karma to watch all the files and run the tests on change ?
Press tab to list possible options.
> yes


Config file generated at "C:\Projects\szkolenia\altkom-js-tdd\jasmine-karma\karma.conf.js".

# Karma start

<!-- global CLI -->
karma start 

<!-- Local scripts -->
<!-- package.json -> scripts -> "test":"karma start"  -->
npm test


## Statyczna Analiza - kompilator TypeScript

npm i -g typescript

tsc --allowJs --noEmit --init --strict false
message TS6071: Successfully created a tsconfig.json file.

"allowJs": ,                             
"checkJs": false,   
"noEmit": true,
"include": [
  "./src/**/*.js",
  "./test/**/*.js",
]

// @ts-check

npm i -D @types/jasmine

## Matchers
https://jasmine.github.io/
https://karma-runner.github.io/6.3/intro/installation.html

https://mochajs.org/
https://www.chaijs.com/
https://devhints.io/jest


https://devhints.io/mocha
https://devhints.io/chai.html
https://devhints.io/mocha-tdd.html
https://www.npmjs.com/package/jasmine-expect

https://devhints.io/jasmine

## Webpack
npm install --save-dev webpack webpack-cli
npm install -D @webpack-cli/generators

npm i @types/jasmine

npm i karma-webpack

npx webpack-cli init

? Which of the following JS solutions do you want to use? Typescript
? Do you want to use webpack-dev-server? Yes
? Do you want to simplify the creation of HTML files for your bundle? Yes
? Do you want to add PWA support? No
? Which of the following CSS solutions do you want to use? CSS only
? Will you be using PostCSS in your project? No
? Do you want to extract CSS for every file? Only for Production
? Do you like to install prettier to format generated configuration? Yes
? Pick a package manager: npm
[webpack-cli] ℹ INFO  Initialising project...
 conflict package.json
? Overwrite package.json? overwrite
    force package.json
   create src\index.ts
   create README.md
   create index.html
   create webpack.config.js
   create tsconfig.json


## Coverage - pokrycie kodu 

npm i istanbul-instrumenter-loader karma-coverage-istanbul-reporter


## React + Jest + TestingLibrary + storybook

npx create-react-app music-app --template=typescript --use-npm
cd music-app
npm i bootstrap
npx sb init 

## Playlists Module

mkdir -p src/playlists/containers/PlaylistsView
touch   src/playlists/containers/PlaylistsView/PlaylistsView.tsx
touch   src/playlists/containers/PlaylistsView/PlaylistsView.test.tsx
touch   src/playlists/containers/PlaylistsView/PlaylistsView.stories.tsx

mkdir -p src/playlists/components/
touch   src/playlists/components/PlaylistDetails.tsx
touch   src/playlists/components/PlaylistDetails.test.tsx
touch   src/playlists/components/PlaylistDetails.stories.tsx

touch   src/playlists/components/PlaylistList.tsx
touch   src/playlists/components/PlaylistList.test.tsx
touch   src/playlists/components/PlaylistList.stories.tsx

touch   src/playlists/components/PlaylistEditor.tsx
touch   src/playlists/components/PlaylistEditor.test.tsx
touch   src/playlists/components/PlaylistEditor.stories.tsx

## Jest + JsDOM

https://github.com/jsdom/jsdom
<!-- No layout, no paint -->

https://create-react-app.dev/docs/debugging-tests
"script":
    "test:dev": "react-scripts test --runInBand --no-cache",
    "test:debug": "react-scripts --inspect-brk test --runInBand --no-cache",

## Tests as spec
npm run test -- --verbose 
Test Suites: 4 passed, 4 total
Tests:       1 todo, 13 passed, 14 total
Snapshots:   0 total
Time:        1.877 sw
Ran all test suites matching /pla/i.
 PASS  src/playlists/containers/PlaylistsView/PlaylistsView.test.tsx
  PlaylistView
    √ shold have "Create new" button (131 ms)
    √ should show empty list with no playlists (11 ms)
    √ should show list of example user playlists (7 ms)
    √ renders PlaylistsView (8 ms)

## Async testing - music search


mkdir -p src/core/model/
touch src/core/model/Search.ts

mkdir -p src/music/containers/MusicSearch
touch   src/music/containers/MusicSearch/MusicSearch.tsx
touch   src/music/containers/MusicSearch/MusicSearch.test.tsx
touch   src/music/containers/MusicSearch/MusicSearch.stories.tsx

mkdir -p src/music/components/
touch   src/music/components/SearchForm.tsx
touch   src/music/components/SearchForm.test.tsx
touch   src/music/components/SearchForm.stories.tsx
touch   src/music/components/SearchResults.tsx
touch   src/music/components/SearchResults.test.tsx
touch   src/music/components/SearchResults.stories.tsx



## Mock data
https://github.com/marak/Faker.js/
https://github.com/boo1ean/casual

http://lorempixel.com/
https://thispersondoesnotexist.com/
https://uifaces.co/?ref=steemhunt
https://uifaces.co/browse-avatars/?ageGroup%5B%5D=youth&gender%5B%5D=male&gender%5B%5D=female&emotion%5B%5D=happiness
https://www.fillmurray.com/

https://snyk.io/


## Angular shallow vs mockComponent
https://github.com/ngneat/spectator

## Mock Service Worker
https://mswjs.io/docs/


## Installing Cypress 

npm install cypress

Installing Cypress (version: 9.0.0)

✔  Downloaded Cypress
✔  Unzipped Cypress
✔  Finished Installation C:\Users\PC\AppData\Local\Cypress\Cache\9.0.0

You can now open Cypress by running: node_modules\.bin\cypress open

https://on.cypress.io/installing-cypress

# Initialization 
npx cypress open
It looks like this is your first time using Cypress: 9.0.0