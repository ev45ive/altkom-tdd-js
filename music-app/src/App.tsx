import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import { Routes, Route } from "react-router-dom";
import { PlaylistsView } from "./playlists/containers/PlaylistsView/PlaylistsView";
import { playlistsMock } from "./playlists/containers/PlaylistsView/playlistsMock";
import { MusicSearch } from "./music/containers/MusicSearch/MusicSearch";

function App() {
  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col">
            <Routes>
              <Route
                path=""
                element={<PlaylistsView playlists={[]} />}
              />
              <Route path="/search" element={<MusicSearch />} />
            </Routes>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
