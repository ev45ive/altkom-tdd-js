// src/core/mocks/handlers.js

// https://mswjs.io/docs/getting-started/mocks/rest-api

import { rest } from 'msw'
import { mockAlbums } from '../../music/containers/MusicSearch/mockAlbums'

export const handlers = [
    // rest.post('/login', (req, res, ctx) => {
    //   // Persist user's authentication in the session
    //   sessionStorage.setItem('is-authenticated', 'true')
    //   return res(
    //     // Respond with a 200 status code
    //     ctx.status(200),
    //   )
    // }),

    rest.get('/search', (req, res, ctx) => {
        const q = (new URLSearchParams(req.url.search)).get('query') || ''

        return res(
            ctx.status(200),
            ctx.json(mockAlbums.filter(a => a.name.includes(q))),
        )
    }),
]