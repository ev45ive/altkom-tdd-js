// tsrafc
import React, { useEffect, useState } from "react";
import { PlaylistDetails } from "../../components/PlaylistDetails";
import { PlaylistEditor } from "../../components/PlaylistEditor";
import { PlaylistList } from "../../components/PlaylistList";
import { Playlist } from "../../../core/model/Playlist";
import { playlistsMock } from "./playlistsMock";
import axios from "axios";

interface Props {
  playlists: Playlist[];
  selectedId?: Playlist["id"];
}

export const PlaylistsView = ({
  selectedId: initialSelected,
  playlists: initialPlaylists,
}: Props) => {
  const [playlists, setPlaylists] = useState(initialPlaylists);
  const [selectedId, setselectedId] = useState(initialSelected);
  const [selected, setSelected] = useState<Playlist | undefined>(undefined);
  const [mode, setMode] = useState<"details" | "edit">("details");

  useEffect(() => {
    axios.get("http://localhost:3000/playlists").then((resp) => {
      setPlaylists(resp.data);
    });
  }, []);

  useEffect(() => {
    setSelected(playlists.find((p) => p.id === selectedId));
  }, [selectedId, playlists]);

  const selectPlaylist = (id: Playlist["id"]) => {
    setselectedId(id);
  };

  const edit = () => {
    setMode("edit");
  };
  const save = (draft: Playlist) => {
    setMode("details");
    setPlaylists((playlists) =>
      playlists.map((p) => (p.id !== draft.id ? p : draft))
    );
  };

  return (
    <div>
      PlaylistsView
      <div className="row">
        <div className="col">
          {/* .list-group>.list-group-item*3{$. Playlist $$} */}
          <div className="list-group">
            <PlaylistList
              playlists={playlists}
              selectedId={selected?.id}
              onSelect={selectPlaylist}
            />

            {/* <div role="button">Create new</div> */}
            {/* <button>Create new</button> */}
            <div>
              <input
                type="button"
                className="btn btn-info float-end my-3"
                value="Create new"
              />
            </div>
          </div>
        </div>
        <div className="col">
          {selected && mode == "details" && (
            <PlaylistDetails playlist={selected} onEdit={edit} />
          )}
          {selected && mode == "edit" && (
            <PlaylistEditor playlist={selected} onSave={save} />
          )}
        </div>
      </div>
    </div>
  );
};
