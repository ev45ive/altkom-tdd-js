import React from "react";
import {
  fireEvent,
  getByText,
  logRoles,
  queryAllByText,
  render,
  screen,
  within,
} from "@testing-library/react";

import { PlaylistsView } from "./PlaylistsView";
import { Playlist } from "../../../core/model/Playlist";
import { playlistsMock } from "./playlistsMock";
import userEvent from "@testing-library/user-event";
describe("PlaylistView", () => {
  const selected = playlistsMock[0];

  const setup = ({
    playlists = playlistsMock,
    selectedId = playlistsMock[0].id,
  }: {
    playlists?: Playlist[];
    selectedId?: Playlist["id"] | null;
  }) => {
    render(
      <PlaylistsView
        selectedId={selectedId || undefined}
        playlists={playlists}
      />
    );
  };

  test('shold have "Create new" button', () => {
    setup({});
    screen.getByRole("button", { name: "Create new" });
  });

  test("should show empty list with no playlists", () => {
    setup({ playlists: [], selectedId: null });
    const listEl = screen.getByTestId("playlists_list");

    getByText(listEl, "No playlists", {
      exact: false,
    });
  });

  test("should show list of example user playlists", () => {
    setup({});
    const listEl = screen.getByTestId("playlists_list");

    playlistsMock.forEach((playlist) => {
      getByText(listEl, playlist.name, {
        exact: false,
      });
    });
  });

  test("renders PlaylistsView", () => {
    setup({});
    const linkElement = screen.getByText(/PlaylistsView/i);
    expect(linkElement).toBeInTheDocument();
  });

  test("higlights selected playlist on list", () => {
    setup({});
    const listEl = screen.getByTestId("playlists_list");
    const elem = getByText(listEl, selected.name, { exact: false });
    expect(elem).toHaveClass("active");
  });

  test("shows selected playlist details", () => {
    setup({ selectedId: playlistsMock[1].id });
    const playlist = playlistsMock[1];
    screen.getByText(playlist.name);
    screen.getByText(/No/);
    screen.getByText(playlist.description);
  });

  test("clicking playlist selects it and shows details", () => {
    // Arrange - Given....
    setup({ selectedId: null });
    const listEl = screen.getByTestId("playlists_list");
    const selected = playlistsMock[1];
    const itemEl = getByText(listEl, selected.name, {
      exact: false,
    });

    // Act - When ...
    fireEvent.click(itemEl);

    // Assert - Then ...
    expect(itemEl).toHaveClass("active");
    const detailsEl = screen.getByTestId("playlist-details");
    getByText(detailsEl, selected.name);
  });

  test("clicking edit in selected playlist details switches to form", () => {
    const selected = playlistsMock[1];
    setup({ selectedId: selected.id });
    const detailsEl = screen.getByTestId("playlist-details");
    const withDetails = within(detailsEl);

    const editBtn = withDetails.getByRole("button", { name: "Edit" });

    fireEvent.click(editBtn);

    expect(screen.queryByTestId("playlist-details")).not.toBeInTheDocument();

    const formEl = screen.getByTestId("playlist-editor");
    const withForm = within(formEl);

    // logRoles(formEl)
    expect(withForm.getByLabelText(/Name/)).toHaveValue(selected.name);
  });

  test("updates playlist on list and details when saved", () => {
    const selected = playlistsMock[1];
    setup({ selectedId: selected.id });

    const editBtn = screen.getByRole("button", { name: "Edit" });

    fireEvent.click(editBtn);
    const formEl = screen.getByTestId("playlist-editor");
    const withForm = within(formEl);
    const nameEl = withForm.getByLabelText(/Name/);
    const saveBtn = withForm.getByRole("button", { name: "Save" });

    userEvent.clear(nameEl);
    userEvent.type(nameEl, "Ala ma kota", {});

    userEvent.click(saveBtn);

    // Close form
    expect(screen.queryByTestId("playlist-editor")).not.toBeInTheDocument();

    const detailsEl = screen.getByTestId("playlist-details");
    const withDetails = within(detailsEl);
    withDetails.getByText(/Ala ma kota/);

    const listEl = screen.getByTestId("playlists_list");
    getByText(listEl, /Ala ma kota/);
  });
});
