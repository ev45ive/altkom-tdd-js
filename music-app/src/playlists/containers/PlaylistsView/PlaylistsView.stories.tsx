import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { PlaylistsView } from './PlaylistsView';
import { playlistsMock } from './playlistsMock';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Playlists/Pages/PlaylistsView',
  component: PlaylistsView,
// More on argTypes: https://storybook.js.org/docs/react/api/argtypes
argTypes: { },
} as ComponentMeta<typeof PlaylistsView>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistsView> = (args) => <PlaylistsView {...args} />;

export const Primary = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  playlists: playlistsMock,
  // selected: playlistsMock[0]
};

export const Selected = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Selected.args = {
  playlists: playlistsMock,
  selectedId: playlistsMock[0].id
};


export const Empty = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Empty.args = {
  playlists: [],
  // selected: playlistsMock[0]
};