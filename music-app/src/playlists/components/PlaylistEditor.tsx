import React, { useState } from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist: Playlist;
  onSave(draft: Playlist): void;
}

export const PlaylistEditor = ({ playlist, onSave }: Props) => {
  const [playlistName, setPlaylistName] = useState(playlist.name);
  const [playlistPublic, setPlaylistPublic] = useState(playlist.public);
  const [playlistDescription, setPlaylistDescription] = useState(
    playlist.description
  );
  const save = () => {
    onSave({
      ...playlist,
      name: playlistName,
      public: playlistPublic,
      description: playlistDescription,
    });
  };
  return (
    <div data-testid="playlist-editor">
      PlaylistEditor
      <div className="form-group my-3">
        <label htmlFor="playlist_name">Name:</label>

        <input
          type="text"
          className="form-control"
          name="playlist_name"
          id="playlist_name"
          placeholder="Name"
          value={playlistName}
          onChange={(e) => setPlaylistName(e.currentTarget.value)}
        />
        <small className="form-text text-muted float-end" data-testid="counter">
          {playlistName.length} / 100
        </small>
      </div>
      <div className="form-check my-3">
        <label className="form-check-label">
          <input
            type="checkbox"
            className="form-check-input"
            name="playlist_public"
            id="playlist_public"
            checked={playlistPublic}
            onChange={(e) => setPlaylistPublic(e.target.checked)}
          />{" "}
          Public
        </label>
      </div>
      <div className="form-group my-3">
        <label htmlFor="playlist_description">Description</label>
        <textarea
          className="form-control"
          name="playlist_description"
          id="playlist_description"
          rows={3}
          value={playlistDescription}
          onChange={(e) => setPlaylistDescription(e.currentTarget.value)}
        ></textarea>
      </div>
      <button className="btn btn-success" onClick={save}>
        Save
      </button>
    </div>
  );
};
