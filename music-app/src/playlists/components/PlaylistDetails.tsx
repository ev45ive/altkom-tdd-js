import React from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist: Playlist;
  onEdit(id: Playlist["id"]): void;
}

export const PlaylistDetails = ({ playlist, onEdit }: Props) => {
  return (
    <div data-testid="playlist-details">
      <dl>
        <dt>Name:</dt>
        <dd>{playlist.name}</dd>

        <dt>Public:</dt>
        <dd
          style={{ borderBottom: "2px solid currentColor" }}
          className={playlist.public ? "text-success" : "text-danger"}
        >
          {playlist.public ? "Yes" : "No"}
        </dd>

        <dt>Description:</dt>
        <dd>{playlist.description}</dd>
      </dl>

      <button className="btn btn-info" onClick={() => onEdit(playlist.id)}>
        Edit
      </button>
    </div>
  );
};
