import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import { PlaylistEditor } from "./PlaylistEditor";
import { playlistsMock } from "../containers/PlaylistsView/playlistsMock";
import userEvent from "@testing-library/user-event";

const setup = () => {
  const saveSpy = jest.fn();
  const selected = playlistsMock[0];
  render(<PlaylistEditor playlist={selected} onSave={saveSpy} />);
  screen.getByTestId("playlist-editor");

  const nameEl = screen.getByLabelText(/Name/);
  const publicEl = screen.getByLabelText(/Public/);
  const descriptionEl = screen.getByLabelText(/Description/);
  const saveBtn = screen.getByRole("button", { name: "Save" });

  return { nameEl, selected, publicEl, descriptionEl, saveBtn, saveSpy };
};

test("renders PlaylistEditor", () => {
  const { nameEl, selected, publicEl, descriptionEl } = setup();

  expect(nameEl).toHaveValue(selected.name);
  expect(publicEl).toBeChecked();
  expect(descriptionEl).toHaveValue(selected.description);
});

test("shows playlist name length counter", () => {
  const { nameEl } = setup();

  const len = (nameEl as HTMLInputElement).value.length;
  expect(screen.getByTestId("counter")).toHaveTextContent(`${len} / 100`);

  userEvent.type(nameEl, "kota", {});

  expect(screen.getByTestId("counter")).toHaveTextContent(
    `${len + "kota".length} / 100`
  );
});

test("updates playlist details", () => {
  const { nameEl, publicEl, descriptionEl } = setup();

  userEvent.clear(nameEl);
  userEvent.type(nameEl, "Ala ma kota", {});

  expect(nameEl).toHaveValue("Ala ma kota");

  expect(publicEl).toBeChecked();
  userEvent.click(publicEl);
  expect(publicEl).not.toBeChecked();

  userEvent.clear(descriptionEl);
  userEvent.type(descriptionEl, "Ala ma opis", {});

  expect(descriptionEl).toHaveValue("Ala ma opis");
});

test("saving changes", () => {
  // const  obj = setup();
  // obj.nameEl
  // obj.descriptionEl
  const { selected, nameEl, publicEl, descriptionEl, saveBtn, saveSpy } = setup();

  userEvent.click(publicEl);
  
  userEvent.clear(nameEl);
  userEvent.type(nameEl, "Ala ma kota", {});
  
  userEvent.clear(descriptionEl);
  userEvent.type(descriptionEl, "Ala ma opis", {});

  // Save button
  userEvent.click(saveBtn);

  // Parent got updated playlist
  expect(saveSpy).toHaveBeenCalledWith(
    expect.objectContaining({
      id: selected.id,
      name: "Ala ma kota",
      description: "Ala ma opis",
      public: false,
    })
  );
});
