import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { PlaylistEditor } from "./PlaylistEditor";
import { playlistsMock } from "../containers/PlaylistsView/playlistsMock";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/PlaylistEditor",
  component: PlaylistEditor,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
} as ComponentMeta<typeof PlaylistEditor>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistEditor> = (args) => (
  <PlaylistEditor {...args} />
);

export const Primary = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  playlist: playlistsMock[0],
};
