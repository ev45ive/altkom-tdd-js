import React from "react";
import { fireEvent, render, screen } from "@testing-library/react";
import { PlaylistDetails } from "./PlaylistDetails";
import { playlistsMock } from "../containers/PlaylistsView/playlistsMock";
import renderer from "react-test-renderer";

it("PlaylistDetails renders correctly", () => {
  const tree = renderer
    .create(<PlaylistDetails playlist={playlistsMock[0]} onEdit={() => {}} />)
    .toJSON();
  expect(tree).toMatchSnapshot();

  const tree2 = renderer
    .create(
      <PlaylistDetails
        playlist={{ ...playlistsMock[0], public: false }}
        onEdit={() => {}}
      />
    )
    .toJSON();
  expect(tree2).toMatchSnapshot("PlaylistDetails with private playlist");
});

test("renders PlaylistDetails", () => {
  render(<PlaylistDetails playlist={playlistsMock[0]} onEdit={() => {}} />);
  const linkElement = screen.getByText(playlistsMock[0].name, { exact: false });
  expect(linkElement).toBeInTheDocument();
});

test("renders PlaylistDetails Public or private", () => {
  const playlist = { ...playlistsMock[0] };
  const { rerender } = render(
    <PlaylistDetails playlist={playlist} onEdit={() => {}} />
  );
  expect(screen.getByText("Yes")).toHaveClass("text-success");

  playlist.public = false;
  rerender(<PlaylistDetails playlist={playlist} onEdit={() => {}} />);
  expect(screen.getByText("No")).toHaveClass("text-danger");
});

test("renders PlaylistDetails", () => {
  const editSpy = jest.fn();
  render(<PlaylistDetails playlist={playlistsMock[0]} onEdit={editSpy} />);
  fireEvent.click(screen.getByRole("button", { name: "Edit" }));
  expect(editSpy).toHaveBeenCalledWith(playlistsMock[0].id);
});
