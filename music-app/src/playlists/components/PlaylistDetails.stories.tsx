import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { PlaylistDetails } from "./PlaylistDetails";
import { playlistsMock } from "../containers/PlaylistsView/playlistsMock";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/PlaylistDetails",
  component: PlaylistDetails,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
} as ComponentMeta<typeof PlaylistDetails>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistDetails> = (args) => (
  <PlaylistDetails {...args} />
);

export const Public = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Public.args = {
  playlist: playlistsMock[0],
};

export const Private = Template.bind({});
Private.args = {
  playlist: { ...playlistsMock[0], public: false },
};
