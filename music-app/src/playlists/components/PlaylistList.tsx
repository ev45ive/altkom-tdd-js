import React from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlists: Playlist[];
  selectedId?: Playlist['id'];
  onSelect(id: Playlist["id"]): void;
}

export const PlaylistList = ({ playlists, selectedId, onSelect }: Props) => {

  return (
    <div data-testid="playlists_list">
      {!playlists?.length && (
        <p className="text-center alert alert-info m-5">No playlists</p>
      )}

      {playlists?.map((playlist, index) => (
        <div
          key={playlist.id}
          className={
            "list-group-item" + (selectedId === playlist.id ? " active" : "")
          }
          onClick={() => onSelect(playlist.id)}
        >
          {index + 1}. {playlist.name}
        </div>
      ))}
    </div>
  );
};
