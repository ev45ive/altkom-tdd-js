import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { PlaylistList } from "./PlaylistList";
import { playlistsMock } from "../containers/PlaylistsView/playlistsMock";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Playlists/PlaylistList",
  component: PlaylistList,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
} as ComponentMeta<typeof PlaylistList>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof PlaylistList> = (args) => (
  <PlaylistList {...args} />
);

export const Empty = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Empty.args = {
  playlists: [],
};


export const Example = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Example.args = {
  playlists: playlistsMock,
};

export const Selected = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Selected.args = {
  playlists: playlistsMock,
  selectedId: playlistsMock[1].id
};
