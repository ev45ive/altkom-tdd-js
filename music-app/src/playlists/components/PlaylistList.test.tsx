import React from "react";
import { fireEvent, getByText, render, screen } from "@testing-library/react";
import { PlaylistList } from "./PlaylistList";
import { playlistsMock } from "../containers/PlaylistsView/playlistsMock";

test("renders empty PlaylistList", () => {
  render(<PlaylistList playlists={[]} onSelect={() => {}} />);
  screen.getByText("No playlists", {
    exact: false,
  });
});

test("renders examples PlaylistList", () => {
  render(<PlaylistList playlists={playlistsMock} onSelect={() => {}} />);
  expect(
    screen.queryByText("No playlists", {
      exact: false,
    })
  ).not.toBeInTheDocument();

  playlistsMock.forEach((playlist) => {
    screen.getByText(playlist.name, {
      exact: false,
    });
  });
});

test("PlaylistList selected", () => {
  const spy = jest.fn();
  // spy.mockReturnValue(null);

  const { rerender } = render(
    <PlaylistList
      playlists={playlistsMock}
      selectedId={playlistsMock[1].id}
      onSelect={spy}
    />
  );
  const item1 = screen.getByText(playlistsMock[1].name, { exact: false });
  expect(item1).toHaveClass("active");

  const item2 = screen.getByText(playlistsMock[2].name, { exact: false });
  expect(item2).not.toHaveClass("active");

  fireEvent.click(item2);
  // expect(item2).toHaveClass("active"); // Delegated to parent

  expect(spy).toHaveBeenCalledWith(playlistsMock[2].id);

  rerender(
    <PlaylistList
      playlists={playlistsMock}
      selectedId={playlistsMock[2].id}
      onSelect={spy}
    />
  );
  const item2clicked = screen.getByText(playlistsMock[2].name, { exact: false });
  expect(item2clicked).toHaveClass("active");
});
