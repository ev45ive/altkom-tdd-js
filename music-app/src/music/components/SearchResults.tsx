import React from "react";
import { Album } from "../containers/MusicSearch/SearchService";

interface Props {
  results: Album[];
}

export const SearchResults = ({ results }: Props) => {
  return (
    <div>
      SearchResults
      <div className="row row-cols-1 row-cols-sm-4 g-0">
        {results.map((result) => (
          <div className="col" key={result.id} data-testid="search-result">
            <div className="card">

              <img src={result.images[0].url} className="card-img-top" />

              <div className="card-body">
                <h5 className="card-title">{result.name}</h5>
              </div>
              
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
