import React, { useState } from "react";

interface Props {
  onSearch(query: string): void;
}

export const SearchForm = ({ onSearch }: Props) => {
  const [query, setQuery] = useState("");

  return (
    <div>
      SearchForm
      <div className="input-group mb-3">
        <input
          type="search"
          className="form-control"
          placeholder="Search"
          onChange={(e) => setQuery(e.currentTarget.value)}
        />

        <button
          className="btn btn-outline-secondary"
          type="submit"
          id="button-search"
          onClick={() => onSearch(query)}
        >
          Search
        </button>
      </div>
    </div>
  );
};
