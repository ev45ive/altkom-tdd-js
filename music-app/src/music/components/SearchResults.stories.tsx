import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import { SearchResults } from './SearchResults';

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: 'Music/SearchResults',
  component: SearchResults,
// More on argTypes: https://storybook.js.org/docs/react/api/argtypes
argTypes: { },
} as ComponentMeta<typeof SearchResults>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof SearchResults> = (args) => <SearchResults {...args} />;

export const Primary = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {
  
};