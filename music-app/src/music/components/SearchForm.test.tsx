import React from 'react';
import { render, screen } from '@testing-library/react';
import { SearchForm } from './SearchForm';

test('renders SearchForm', () => {
  render(<SearchForm  />);
  const linkElement = screen.getByText(/SearchForm/i);
  expect(linkElement).toBeInTheDocument();
});