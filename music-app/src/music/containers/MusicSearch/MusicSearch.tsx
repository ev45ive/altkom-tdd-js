import React, { useState } from "react";
import { SearchForm } from "../../components/SearchForm";
import { SearchResults } from "../../components/SearchResults";
import { Album, searchService } from "./SearchService";

interface Props {}

export const MusicSearch = (props: Props) => {
  const [results, setResults] = useState<Album[]>([]);

  const search = (query: string) => {
    searchService.fetchSearchResults(query).then((res) => {
      setResults(res);
    });
  };

  return (
    <div>
      MusicSearch
      <div className="row">
        <div className="col">
          <SearchForm onSearch={search} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <SearchResults results={results} />
        </div>
      </div>
    </div>
  );
};
