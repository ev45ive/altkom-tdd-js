import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { MusicSearch } from "./MusicSearch";
import { handlers } from "../../../core/mocks/handlers";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "Music/Pages/MusicSearch",
  component: MusicSearch,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {},
} as ComponentMeta<typeof MusicSearch>;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: ComponentStory<typeof MusicSearch> = (args) => (
  <MusicSearch {...args} />
);

export const Primary = Template.bind({});

// More on args: https://storybook.js.org/docs/react/writing-stories/args
Primary.args = {};

Primary.parameters = {
  msw: handlers,
  // msw: [
  //   rest.get('/user', (req, res, ctx) => {
  //     return res(
  //       ctx.json({
  //         firstName: 'Neil',
  //         lastName: 'Maverick',
  //       })
  //     )
  //   }),
  // ],
};
