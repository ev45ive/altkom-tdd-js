import React from "react";
import { render, screen, logDOM, waitFor } from "@testing-library/react";
import { MusicSearch } from "./MusicSearch";
import { mocked } from "ts-jest/utils";

import { SearchForm } from "../../components/SearchForm";
import userEvent from "@testing-library/user-event";
import { Album, searchService } from "./SearchService";
import { act } from "react-dom/test-utils";
import { mockAlbums } from "./mockAlbums";

// jest.mock("./SearchService");

describe("MusicSearch", () => {
  const setup = () => {
    render(<MusicSearch />);
    const searchbox = screen.getByRole("searchbox");
    const searchbtn = screen.getByRole("button", { name: "Search" });
    return { searchbox, searchbtn };
  };
  test("renders MusicSearch", () => {
    setup();

    const linkElement = screen.getByText(/MusicSearch/i);
    expect(linkElement).toBeInTheDocument();

    // screen.debug();
  });

  test("has empty search bar, search button and no results", () => {
    const { searchbox, searchbtn } = setup();

    expect(searchbox).toHaveValue("");
    expect(screen.queryAllByTestId("search-result")).toHaveLength(0);
  });

  test("should search for music and should show search results", async () => {
    const { searchbox, searchbtn } = setup();
    // const serviceMock = mocked(searchService.fetchSearchResults);

    // serviceMock.mockResolvedValue(mockAlbums as Album[]);
    const query = "batman";

    userEvent.type(searchbox, query);
    userEvent.click(searchbtn);
    // expect(serviceMock).toHaveBeenCalledWith(query);

    expect(await screen.findAllByTestId("search-result")).toHaveLength(mockAlbums.length);
  });
});

// Warning: An update to MusicSearch inside a test was not wrapped in act(...).

//     When testing, code that causes React state updates should be wrapped into act(...):

//     act(() => {
//       /* fire events that update state */
//     });
//     /* assert on the output */

// jest.mock("../../components/SearchForm");
// jest.mock("../../components/SearchForm", () => ({
//   SearchForm({ onSearch = jest.fn() }) {
//     return "tutaj bedzie searchbox";
//   },
// }));

// test("should search for music", () => {
//   // (SearchForm as jest.MockedFunction<typeof SearchForm>).mockReturnValue(
//   mocked(SearchForm).mockReturnValue(<p>tutaj searchbox</p>);

//   render(<MusicSearch />);

//   const search = mocked(SearchForm).mock.calls[0][0].onSearch;
//   search("batman");

// });
