import axios from "axios";

export type Album = {
  id: string;
  name: string;
  images: {
    url: string;
  }[];
};
export class SearchService {
  async fetchSearchResults(query: string): Promise<Album[]> {
    const { data } = await axios.get<Album[]>("/search", {
      params: { query },
    });

    return data;
  }
}
export const searchService = new SearchService();
