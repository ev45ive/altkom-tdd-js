/// <reference types="@testing-library/cypress" />
/// <reference path="../../../src/react-app-env.d.ts" />


import playlists from '../../fixtures/playlists.json'

// playlists[0].name // string

describe('Playlists View', () => {

    beforeEach(() => {
        cy.intercept('http://localhost:3000/playlists', { fixture: 'playlists.json' })
            .as('playlistsReq')

        cy.fixture('playlists').as('playlists').then(function (this: any, playlists) {
            this.playlists = playlists
        })
        cy.visit('http://localhost:3000/')

        cy.wait('@playlistsReq')

        cy.get('[data-testid=playlists_list]').as('list-view')
    })

    it('Shows playlists', function () {

        // cy.get('@playlists').then(playlists => {
        //     cy.get('[data-testid=playlists_list] > *')
        //         .should('have.length', playlists.length)
        // })

        cy.get('[data-testid=playlists_list] > *')
            .should('have.length', this.playlists.length)
            .first()
            .should('contain.text', "Playlist 123")
    })
    it('Selects playlist', () => {
        // cy.get('[data-testid=playlists_list] > *')
        //     .should('have.length', 3)
        //     .eq(1)
        //     .should('contain.text', "Playlist 234")
        //

        cy.get('@list-view')
            .contains('Playlist 234')
            .click()
            .should('have.class', 'active')
    })

    describe('with Playlist 234 selected ', () => {
        beforeEach(() => {
            cy.get('@list-view')
                .contains('Playlist 234')
                .click()
                .should('have.class', 'active')

            cy.get('[data-testid=playlist-details]').as('details')
        })

        it('should show details', () => {
            cy.get('@details').within(($detailsElem) => {

                cy.contains('Playlist 234').then(($el: JQuery<HTMLElement>) => {
                    // imperative jquery code
                    $el.css('color', 'hotpink')
                })
                cy.contains('No')
                cy.contains('Best playlist ever 234')
            })
        })

        it('should show edit form', () => {
            cy.get('@details')
                .get('button')
                .should('have.text', 'Edit')
                .click()

            cy.get('[data-testid="playlist-editor"]').within(() => {
                cy.findByLabelText(/Name/).should('have.value', 'Playlist 234')
                cy.findByLabelText('Public').should('not.be.checked')
                cy.findByLabelText('Description').should('have.value', 'Best playlist ever 234')
            })
        })
    })
})
