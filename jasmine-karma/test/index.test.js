
describe('Testing module', () => {

    it('tests tests are testing', () => {
        expect(1 + 2).toEqual(3)
    })

    it('more tests are testing', () => {
        // Chrome 95.0.4638.69 (Windows 10) Testing module more tests are testing FAILED
        // expect(1 + 2).toEqual(-1)
        expect(1 - 2).toEqual(-1)
        if (1 > 2) throw new Error('Upss..')
    })
})