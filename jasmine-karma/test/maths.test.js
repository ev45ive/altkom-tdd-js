// @ts-check
describe('Maths module', () => {

    it('should only add numbers', () => {
        expect(() => add(123, 213)).not.toThrow()
        // Argument of type 'string' is not assignable to parameter of type 'number'
        // expect(() => add('5kg jablek', '2 samoloty')).toThrow() 
        // expect(() => add('5kg jablek', 123)).toThrow()
    })

    it('adding numbers', () => {
        expect(add(1, 2)).toEqual(3)
        expect(add(0, 5)).toEqual(5)
        expect(add(0, -5)).toEqual(-5)
    })

    // fit(...) // focus only this test, skip rest
    // xit(...) // skip this test

    it('adding numbers with 6 decimal points precision', () => {
        // Error: Expected 0.30000000000000004 to equal 0.3.
        expect(add(0.2, 0.1)).toEqual(0.3) // IEEE 754
        expect(add(0.123456, 0.654321)).toEqual(0.777777) // IEEE 754
        expect(add(0.1234564, 0.6543210)).toEqual(0.777777) // IEEE 754
        expect(add(0.1234567, 0.6543210)).toEqual(0.777778) // IEEE 754
    })

    it('substracting numbers', () => {
        expect(substract(1, 2)).toEqual(-1)
        expect(substract(0, 3)).toEqual(-3)
    })


    it('substracting numbers with 6 decimal points precision', () => {
        // Error: Expected 0.30000000000000004 to equal 0.3.
        expect(substract(0.3, 0.2)).toEqual(0.1) // IEEE 754
        expect(substract(0.654321, 0.111111)).toEqual(0.543210) // IEEE 754
    })
})

