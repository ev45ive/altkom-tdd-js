// @ts-check

// @ts-ignore
function normalizeFloats(result) {
    return parseFloat((result).toFixed(6));
}

/**
 * Adding numbers with decimal float
 *
 * @param {number} a
 * @param {number} b
 * @return {number}
 */
function add(a, b) {
    if (typeof a !== 'number' || typeof b !== 'number') { throw 'Only numbers'; }
    return normalizeFloats(a + b);
}

// // @ts-expect-error

function substract(a, b) {
    return normalizeFloats(a - b);
}
